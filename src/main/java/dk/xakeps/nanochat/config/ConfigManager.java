package dk.xakeps.nanochat.config;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.commented.SimpleCommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;

import java.io.IOException;

public class ConfigManager {
    private final ConfigurationLoader<CommentedConfigurationNode> configLoader;
    private Config config;

    public ConfigManager(ConfigurationLoader<CommentedConfigurationNode> configLoader) throws IOException, ObjectMappingException {
        this.configLoader = configLoader;
        this.config = new Config();
        reloadConfig();
    }

    public void reloadConfig() throws ObjectMappingException, IOException {
        CommentedConfigurationNode load = configLoader.load(ConfigurationOptions.defaults().setShouldCopyDefaults(true));
        config = load.getValue(TypeToken.of(Config.class), new Config());
        configLoader.save(load); // save if file was removed
    }

    public void saveConfig() {
        SimpleCommentedConfigurationNode root = SimpleCommentedConfigurationNode.root(ConfigurationOptions.defaults().setShouldCopyDefaults(true));
        try {
            root.setValue(TypeToken.of(Config.class), getConfig());
            configLoader.save(root);
        } catch (ObjectMappingException | IOException e) {
            throw new RuntimeException("Error while saving config", e);
        }
    }

    public Config getConfig() {
        if (config == null) {
            throw new IllegalStateException("Config is null!");
        }
        return config;
    }
}
