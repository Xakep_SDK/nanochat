/*
 * This file is part of NanoChat.
 *
 * NanoChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NanoChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NanoChat.  If not, see <https://www.gnu.org/licenses/>.
 */
package dk.xakeps.nanochat.config;

import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Objects;

@ConfigSerializable
public class Config {
    @Setting(comment = "Set to true, if plugin should be enabled, false otherwise")
    private boolean enabled = true;
    @Setting(comment = "Local chat range, in blocks")
    private int range = 128;
    @Setting(comment = "Global chat prefix")
    private Text globalPrefix = Text.of(TextColors.GREEN, "[G] ");
    @Setting(comment = "Local chat prefix")
    private Text localPrefix = Text.of(TextColors.GREEN, "[L] ");
    @Setting(comment = "Should modify message body? (remove '!' before message)")
    private boolean modifyBody = true;
    @Setting(comment = "Run before other plugins or after. Possible values: LATE, EARLY.")
    private Order order = Order.LATE;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public Text getGlobalPrefix() {
        return globalPrefix;
    }

    public void setGlobalPrefix(Text globalPrefix) {
        this.globalPrefix = globalPrefix;
    }

    public Text getLocalPrefix() {
        return localPrefix;
    }

    public void setLocalPrefix(Text localPrefix) {
        this.localPrefix = localPrefix;
    }

    public boolean isModifyBody() {
        return modifyBody;
    }

    public void setModifyBody(boolean modifyBody) {
        this.modifyBody = modifyBody;
    }

    public Order getOrder() {
        if (order != Order.LATE && order != Order.EARLY) {
            throw new IllegalArgumentException("Order can be only EARLY or LATE!");
        }
        return order;
    }

    public void setOrder(Order order) {
        if (order != Order.LATE && order != Order.EARLY) {
            throw new IllegalArgumentException("Order can be only EARLY or LATE!");
        }
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Config config = (Config) o;
        return enabled == config.enabled &&
                range == config.range &&
                modifyBody == config.modifyBody &&
                Objects.equals(globalPrefix, config.globalPrefix) &&
                Objects.equals(localPrefix, config.localPrefix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enabled, range, globalPrefix, localPrefix, modifyBody);
    }
}
