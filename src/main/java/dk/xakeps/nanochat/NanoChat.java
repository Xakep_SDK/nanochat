/*
 * This file is part of NanoChat.
 *
 * NanoChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NanoChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NanoChat.  If not, see <https://www.gnu.org/licenses/>.
 */
package dk.xakeps.nanochat;

import dk.xakeps.nanochat.command.PrefixGetCommandElement;
import dk.xakeps.nanochat.command.PrefixSetCommandElement;
import dk.xakeps.nanochat.config.ConfigManager;
import dk.xakeps.nanochat.listener.ChatListener;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Plugin(id = "nanochat",
        name = "NanoChat",
        version = "2.2",
        description = "Simple local and global chat",
        url = "https://spongeapi.com",
        authors = "Xakep_SDK")
public class NanoChat {
    private final PluginContainer container;
    private final ConfigManager configManager;
    private final ChatListener listener;

    @Inject
    public NanoChat(PluginContainer container,
                    @DefaultConfig(sharedRoot = true) ConfigurationLoader<CommentedConfigurationNode> configLoader)
            throws IOException, ObjectMappingException {
        this.container = container;
        this.configManager = new ConfigManager(configLoader);
        this.listener = new ChatListener(configManager::getConfig);
    }

    @Listener
    public void onGamePreInit(GamePreInitializationEvent event) {
        reloadListener();
        CommandSpec localPrefixGetCommand = CommandSpec.builder()
                .permission("nanochat.command.prefix.local.get")
                .executor(new PrefixGetCommandElement(configManager.getConfig()::getLocalPrefix, Text.of("local")))
                .build();
        CommandSpec localPrefixSetCommand = CommandSpec.builder()
                .permission("nanochat.command.prefix.local.set")
                .arguments(PrefixSetCommandElement.ARGUMENT)
                .executor(new PrefixSetCommandElement(configManager, configManager.getConfig()::setLocalPrefix, Text.of("local")))
                .build();
        CommandSpec localPrefixCommand = CommandSpec.builder()
                .permission("nanochat.command.prefix.local")
                .child(localPrefixGetCommand, "get")
                .child(localPrefixSetCommand, "set")
                .build();

        CommandSpec globalPrefixGetCommand = CommandSpec.builder()
                .permission("nanochat.command.prefix.global.get")
                .executor(new PrefixGetCommandElement(configManager.getConfig()::getGlobalPrefix, Text.of("global")))
                .build();
        CommandSpec globalPrefixSetCommand = CommandSpec.builder()
                .permission("nanochat.command.prefix.global.set")
                .arguments(PrefixSetCommandElement.ARGUMENT)
                .executor(new PrefixSetCommandElement(configManager, configManager.getConfig()::setGlobalPrefix, Text.of("local")))
                .build();
        CommandSpec globalPrefix = CommandSpec.builder()
                .permission("nanochat.command.prefix.global")
                .child(globalPrefixGetCommand, "get")
                .child(globalPrefixSetCommand, "set")
                .build();

        CommandSpec prefixCommand = CommandSpec.builder()
                .permission("nanochat.command.prefix")
                .child(localPrefixCommand, "local")
                .child(globalPrefix, "global")
                .build();

        CommandSpec getRangeCommand = CommandSpec.builder()
                .permission("nanochat.command.range.get")
                .executor((src, args) -> {
                    src.sendMessage(Text.of("Local chat range: ", configManager.getConfig().getRange()));
                    return CommandResult.success();
                }).build();
        CommandSpec setRangeCommand = CommandSpec.builder()
                .permission("nanochat.command.range.set")
                .arguments(GenericArguments.integer(Text.of("range")))
                .executor((src, args) -> {
                    int range = args.<Integer>getOne(Text.of("range")).get();
                    configManager.getConfig().setRange(range);
                    src.sendMessage(Text.of(TextColors.GREEN, "Local chat range changed. New value: ", TextColors.GOLD, range));
                    configManager.saveConfig();
                    return CommandResult.success();
                }).build();
        CommandSpec rangeCommand = CommandSpec.builder()
                .permission("nanochat.command.range")
                .child(getRangeCommand, "get")
                .child(setRangeCommand, "set")
                .build();

        CommandSpec getOrderCommand = CommandSpec.builder()
                .permission("nanochat.command.order.get")
                .executor((src, args) -> {
                    src.sendMessage(Text.of(TextColors.GREEN, "Listener order: ", TextColors.GOLD, configManager.getConfig().getOrder()));
                    return CommandResult.success();
                }).build();
        Map<String, Order> choices = new HashMap<>(2);
        choices.put(Order.EARLY.name(), Order.EARLY);
        choices.put(Order.LATE.name(), Order.LATE);
        CommandSpec setOrderCommand = CommandSpec.builder()
                .permission("nanochat.command.order.set")
                .arguments(GenericArguments.choices(Text.of("order"), choices))
                .executor((src, args) -> {
                    Order order = args.<Order>getOne(Text.of("order")).get();
                    configManager.getConfig().setOrder(order);
                    configManager.saveConfig();
                    reloadListener();
                    src.sendMessage(Text.of(TextColors.GREEN, "Listener order changed. New value: ", TextColors.GOLD, order));
                    return CommandResult.success();
                }).build();
        CommandSpec orderCommand = CommandSpec.builder()
                .permission("nanochat.command.order")
                .child(getOrderCommand, "get")
                .child(setOrderCommand, "set")
                .build();

        CommandSpec getModifyBodyCommand = CommandSpec.builder()
                .permission("nanochat.command.modifybody.get")
                .executor((src, args) -> {
                    src.sendMessage(Text.of(TextColors.GREEN, "Modify body: ", TextColors.GOLD, configManager.getConfig().isModifyBody()));
                    return CommandResult.success();
                }).build();
        CommandSpec setModifyBodyCommand = CommandSpec.builder()
                .permission("nanochat.command.modifybody.set")
                .arguments(GenericArguments.bool(Text.of("modifyBody")))
                .executor((src, args) -> {
                    boolean modifyBody = args.<Boolean>getOne(Text.of("modifyBody")).get();
                    configManager.getConfig().setModifyBody(modifyBody);
                    configManager.saveConfig();
                    src.sendMessage(Text.of(TextColors.GREEN, "Modify body value changed. New value: ", TextColors.GOLD, modifyBody));
                    return CommandResult.success();
                }).build();
        CommandSpec modifyBodyCommand = CommandSpec.builder()
                .permission("nanochat.command.modifybody")
                .child(getModifyBodyCommand, "get")
                .child(setModifyBodyCommand, "set")
                .build();

        CommandSpec getEnabledCommand = CommandSpec.builder()
                .permission("nanochat.command.enabled.get")
                .executor((src, args) -> {
                    src.sendMessage(Text.of(TextColors.GREEN, "Plugin enabled: ", TextColors.GOLD, configManager.getConfig().isEnabled()));
                    return CommandResult.success();
                }).build();
        CommandSpec setEnabledCommand = CommandSpec.builder()
                .permission("nanochat.command.enabled.set")
                .arguments(GenericArguments.bool(Text.of("enabled")))
                .executor((src, args) -> {
                    boolean enabled = args.<Boolean>getOne(Text.of("enabled")).get();
                    configManager.getConfig().setEnabled(enabled);
                    configManager.saveConfig();
                    src.sendMessage(Text.of(TextColors.GREEN, "Plugin enabled status changed. New value: ", TextColors.GOLD, enabled));
                    return CommandResult.success();
                }).build();
        CommandSpec enabledCommand = CommandSpec.builder()
                .permission("nanochat.command.enabled")
                .child(getEnabledCommand, "get")
                .child(setEnabledCommand, "set")
                .build();

        CommandSpec rootCommand = CommandSpec.builder()
                .permission("nanochat.command")
                .child(prefixCommand, "prefix")
                .child(rangeCommand, "range")
                .child(orderCommand, "order")
                .child(modifyBodyCommand, "modifybody")
                .child(enabledCommand, "enabled")
                .build();

        Sponge.getCommandManager().register(container, rootCommand, "nanochat", "nc");
    }

    @Listener
    public void onPluginReload(GameReloadEvent event) throws IOException, ObjectMappingException {
        configManager.reloadConfig();
        reloadListener();
    }

    private void reloadListener() {
        Sponge.getEventManager().unregisterListeners(listener);
        Order order = configManager.getConfig().getOrder();
        boolean beforeModifications = order == Order.EARLY;
        Sponge.getEventManager().registerListener(container, MessageChannelEvent.Chat.class, order, beforeModifications, listener);
    }
}
