/*
 * This file is part of NanoChat.
 *
 * NanoChat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NanoChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NanoChat.  If not, see <https://www.gnu.org/licenses/>.
 */
package dk.xakeps.nanochat.command;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.function.Supplier;

public class PrefixGetCommandElement implements CommandExecutor {
    private final Supplier<Text> prefixGetter;
    private final Text chatType;

    public PrefixGetCommandElement(Supplier<Text> prefixGetter, Text chatType) {
        this.prefixGetter = prefixGetter;
        this.chatType = chatType;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        src.sendMessage(Text.of(TextColors.GREEN, "Prefix for ", TextColors.GOLD, chatType,
                TextColors.GREEN, " is: ", TextColors.RESET, prefixGetter.get()));
        return CommandResult.success();
    }
}
